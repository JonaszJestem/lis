### Longest increasing subsequence
![pipeline](https://gitlab.com/JonaszJestem/lis/badges/master/pipeline.svg)

Repository contains solution for recruitment task: 
```
This is the list of random numbers:
[1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16]
Remove items from the list in such a way that the rest items were sorted ascending and the list was as long as possible.

The solution for the above array is:
[1, 3, 7, 10, 12, 16]

The implementation should:
- be a function, take an array and return an array
- not use external modules 
```

### Solution

The solution for this problem is to find longest subsequence of given array with numbers that are not decreasing.
My implementation used modified Kadane algorithm, that gives the solution with complexity of `O(n^2)`. 
There are also solutions with `O(nlog(n))` complexity, but they require writing more complex and hard to read code.

As there was no requirement to do it as fast as possible, I choose simpler polynomial solution which is easier to understand.
 
