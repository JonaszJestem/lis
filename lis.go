package lis

//Function returns longest non-decreasing subsequence of given array
func LongestIncreasingSubsequence(numbers []int) []int {
	if len(numbers) == 0 {
		return []int{}
	}

	previousElement, lisLength, indexOfLastElement := calculateLongestIncreasingSubsequence(numbers)

	return reconstructSequence(lisLength, numbers, indexOfLastElement, previousElement)
}

func reconstructSequence(lisLength int, numbers []int, indexOfLastElement int, previousElement []int) []int {
	longestSequence := make([]int, lisLength)
	for i := lisLength - 1; i >= 0; i-- {
		longestSequence[i] = numbers[indexOfLastElement]
		indexOfLastElement = previousElement[indexOfLastElement]
	}
	return longestSequence
}

func calculateLongestIncreasingSubsequence(numbers []int) ([]int, int, int) {
	var maxEndingAt = make([]int, len(numbers))
	var previousElement = make([]int, len(numbers))
	maxEndingAt[0] = 1

	for currentIndex := 1; currentIndex < len(numbers); currentIndex++ {
		calculateMaxAt(maxEndingAt, currentIndex, numbers, previousElement)
	}

	lisLength, indexOfLastElement := maxFrom(maxEndingAt)
	return previousElement, lisLength, indexOfLastElement
}

func calculateMaxAt(maxEndingAt []int, currentIndex int, numbers []int, previousElement []int) {
	maxEndingAt[currentIndex] = 1
	for previousIndex := 0; previousIndex < currentIndex; previousIndex++ {
		if formsLongerIncreasingSequence(numbers, currentIndex, previousIndex, maxEndingAt) {
			maxEndingAt[currentIndex] = maxEndingAt[previousIndex] + 1
			previousElement[currentIndex] = previousIndex
		}
	}
}

func formsLongerIncreasingSequence(numbers []int, currentIndex int, previousIndex int, maxEndingAt []int) bool {
	return isIncreasing(numbers, currentIndex, previousIndex) && isLongerSequence(maxEndingAt, currentIndex, previousIndex)
}

func isLongerSequence(lis []int, currentIndex int, previousIndex int) bool {
	return lis[currentIndex] < lis[previousIndex]+1
}

func isIncreasing(numbers []int, currentIndex int, previousIndex int) bool {
	return numbers[currentIndex] >= numbers[previousIndex]
}

func maxFrom(slice []int) (int, int) {
	currentMax := slice[0]
	maxIndex := 0
	for i, currentElement := range slice {
		if currentElement > currentMax {
			currentMax = currentElement
			maxIndex = i
		}
	}
	return currentMax, maxIndex
}
