package lis

import (
	"reflect"
	"testing"
)

func TestMainCase(t *testing.T) {
	numbers := []int{1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16}
	expected := []int{1, 5, 7, 10, 14, 16}

	actual := LongestIncreasingSubsequence(numbers)

	assertEqual(t, expected, actual)
}

func TestEmptyArray(t *testing.T) {
	var numbers, expected []int

	actual := LongestIncreasingSubsequence(numbers)

	assertEqual(t, expected, actual)
}

func TestOneElementArray(t *testing.T) {
	numbers, expected := []int{1}, []int{1}

	actual := LongestIncreasingSubsequence(numbers)

	assertEqual(t, expected, actual)
}

func TestDecreasingElements(t *testing.T) {
	numbers, expected := []int{5, 4, 3}, [][]int{{5}, {4}, {3}}

	actual := LongestIncreasingSubsequence(numbers)

	assertOneOfExpected(t, expected, actual)
}

func TestAllTheSameNumbers(t *testing.T) {
	numbers, expected := []int{5, 5, 5}, []int{5, 5, 5}

	actual := LongestIncreasingSubsequence(numbers)

	assertEqual(t, expected, actual)
}

func assertOneOfExpected(t *testing.T, expectedNumbers [][]int, actual []int) {
	for _, expected := range expectedNumbers {
		if reflect.DeepEqual(expected, actual) {
			return
		}
	}
	t.Errorf("Expected one of %v, but got %v", expectedNumbers, actual)
}

func assertEqual(t *testing.T, expected []int, actual []int) {
	if len(expected) == 0 && len(actual) == 0 {
		return
	}
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Expected %v, but got %v", expected, actual)
	}
}
